const contacts = [
    {
        name: 'Richard',
        type: 'centrakor',
        mail: 'richard.dequeker@mail.com',
        phone: '0684068406'
    },
    {
        name: 'George',
        type: 'autres',
        mail: 'george.kevin@mail.com',
        phone: '0684068406'
    },
    {
        name: 'Greg',
        type: 'enseigne',
        mail: 'greg.kevin@mail.com',
        phone: '012908920'
    },
];

function compareObjects(o1, o2) {
    let k = '';
    for (k in o1) if (o1[k] != o2[k]) return false;
    for (k in o2) if (o1[k] != o2[k]) return false;
    return true;
}

function itemExists(haystack, needle) {
    for (let i = 0; i < haystack.length; i++) if (compareObjects(haystack[i], needle)) return true;
    return false;
}


function searchInArrayOfObjects(arrayOfObjects, toSearch) {
    const results = [];
    toSearch = toSearch.trim(); // trim it
    for (var i = 0; i < arrayOfObjects.length; i++) {
        for (var key in arrayOfObjects[i]) {
            if (arrayOfObjects[i][key].toLowerCase().indexOf(toSearch) != -1) {
                if (!itemExists(results, arrayOfObjects[i])) results.push(arrayOfObjects[i]);
            }
        }
    }
    return results;
}

console.log(searchInArrayOfObjects(contacts, 'kev'));